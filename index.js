import {AppRegistry} from 'react-native';
import HomePage from './src/pages/HomePage/HomePage';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => HomePage);
