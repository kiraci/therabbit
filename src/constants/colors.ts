const Colors = {
  // Dark Palette
  grey50: '#FAFAFA',
  grey100: '#F5F5F5',
  grey200: '#EEEEEE',
  grey300: '#E0E0E0',
  grey400: '#BDBDBD',
  grey500: '#9E9E9E',
  grey600: '#757575',
  grey700: '#616161',
  grey800: '424242',
  grey900: '#212121',

  // Primary Palette
  indigo50: '#E8EAF6',
  indigo100: '#C5CAE9',
  indigo200: '#9FA8DA',
  indigo300: '#7986CB',
  indigo400: '#5C6BC0',
  indigo500: '#3F51B5',
  indigo600: '#3949AB',
  indigo700: '#303F9F',
  indigo800: '#283593',
  indigo900: '#1A237E',
  indigoA100: '#8C9EFF',
  indigoA200: '#536DFE',
  indigoA400: '#3D5AFE',
  indigoA700: '#304FFE',
};

export default Colors;
