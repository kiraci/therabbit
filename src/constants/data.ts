// Create habit data
const habitData = [
  {
    header: 'Workout',
    icon: 'workout',
    subheader: '30 minutes',
    description: 'Do 30 minutes of cardio',
  },
  {
    header: 'Study',
    icon: 'study',
    subheader: '1 hour',
    description: 'Study for 1 hour',
  },
  {
    header: 'Reading',
    icon: 'reading',
    subheader: '30 minutes',
    description: 'Read 10 pages',
  },
];

export default habitData;
