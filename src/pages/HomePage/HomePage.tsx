import React from 'react';
import {Dimensions, SafeAreaView, ScrollView, View} from 'react-native';
import styles from './HomeStyle';
import Header from '../../components/atoms/Header/Header';
import FloatingButton from '../../components/atoms/FloatingButton/FloatingButton';
import Colors from '../../constants/colors';
import {Carousel} from 'react-native-basic-carousel';
import habitData from '../../constants/data';
import IntervalCardSlider from '../../components/organisms/IntervalCardSlider/IntervalCardSlider';
import TemplateCard from '../../components/molecules/TemplateCard/TemplateCard';

const HomePage = () => {
  const windowWidth = Dimensions.get('window').width;
  //const windowHeight = Dimensions.get('window').height;

  return (
    <SafeAreaView style={styles.backgroundStyle}>
      {/** Brand Header */}
      <View style={styles.header}>
        <Header
          header={'The Rabbit'}
          float={'left'}
          color={'white'}
          fontSize={26}
        />
        <FloatingButton
          size={42}
          color={Colors.indigo700}
          initial={'O'}
          right={10}
          top={8}
        />
      </View>

      {/** Scrollable List View */}
      <View>
        <ScrollView style={styles.scrollStyle}>
          <View style={styles.scrollviewStyle}>
            <View>
              <Carousel
                data={habitData}
                renderItem={({item}) => (
                  <TemplateCard
                    header={item.header}
                    subheader={item.subheader}
                    image={item.icon}
                  />
                )}
                itemWidth={windowWidth - 15 * 2}
                onSnapToItem={item => console.log(item)}
                pagination
                autoplay
              />
            </View>

            <IntervalCardSlider
              header={'Habits'}
              data={habitData}
              type={'IntervalCard'}
            />

            <IntervalCardSlider
              header={'Quick Start'}
              headerStyle={{
                color: Colors.grey300,
                textAlign: 'center',
              }}
              subheaderStyle={{
                color: Colors.grey300,
                textAlign: 'center',
              }}
              borderColor={'#FF7043'}
              data={habitData}
              type={'QuickStartCard'}
            />

            <IntervalCardSlider
              header={'History'}
              data={habitData}
              type={'IntervalCard'}
            />
          </View>
        </ScrollView>
      </View>

      {/** Floating Button To Start or Create Interval*/}
      <FloatingButton
        size={50}
        color={Colors.indigo700}
        icon="start"
        right={20}
        bottom={20}
      />
    </SafeAreaView>
  );
};

export default HomePage;
