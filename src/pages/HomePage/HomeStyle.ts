import {StyleSheet} from 'react-native';
import Colors from '../../constants/colors';

const styles = StyleSheet.create({
  backgroundStyle: {
    backgroundColor: Colors.grey900,
    flex: 1,
  },
  header: {
    borderBottomColor: Colors.grey400,
    borderBottomWidth: 1,
    padding: 15,
  },
  scrollStyle: {},
  scrollviewStyle: {
    padding: 15,
    marginBottom: 75,
  },
});

export default styles;
