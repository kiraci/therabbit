import React from 'react';
import {ImageBackground, Pressable, Text} from 'react-native';
import ITemplateCard from './ITemplateCard';
import createStyles from './TemplateCardStyle';

const TemplateCard = (props: ITemplateCard) => {
  // Create styles
  const styles = createStyles();

  // Get image path
  const imagePath = () => {
    switch (props.image) {
      case 'workout':
        return require('../../../assets/images/background-workout.jpg');
      case 'study':
        return require('../../../assets/images/background-study.jpg');
      case 'reading':
        return require('../../../assets/images/background-reading.jpg');
      default:
        return require('../../../assets/images/default-interval.png');
    }
  };

  return (
    <Pressable style={styles.container}>
      <ImageBackground
        source={imagePath()}
        resizeMode={'cover'}
        style={styles.image}
        imageStyle={styles.borderRadius}
      />
      <Text style={styles.header}>{props.header}</Text>
      <Text style={styles.subheader}>{props.subheader}</Text>
    </Pressable>
  );
};

export default TemplateCard;
