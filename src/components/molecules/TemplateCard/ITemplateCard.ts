interface ITemplateCard {
  header: string;
  subheader: string;
  image: string;
}

export default ITemplateCard;
