import {StyleSheet} from 'react-native';

const createStyles = () =>
  StyleSheet.create({
    container: {
      height: 150,
    },
    image: {
      flex: 1,
      justifyContent: 'center',
      opacity: 0.8,
    },
    header: {
      fontSize: 20,
      fontWeight: 'bold',
      color: 'white',
      textAlign: 'center',
      position: 'absolute',
      left: 20,
      bottom: 20,
    },
    subheader: {
      fontSize: 12,
      color: 'white',
      textAlign: 'center',
      position: 'absolute',
      left: 20,
      bottom: 45,
    },
    borderRadius: {
      borderRadius: 20,
    },
  });

export default createStyles;
