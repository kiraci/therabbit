import {StyleSheet} from 'react-native';

const createStyles = () =>
  StyleSheet.create({
    container: {
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      padding: 10,
      backgroundColor: 'black',
      margin: 10,
      marginLeft: 0,
      borderRadius: 10,
      minWidth: 150,
    },
  });

export default createStyles;
