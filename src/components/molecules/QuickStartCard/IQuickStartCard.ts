interface IQuickStartCard {
  header: string;
  headerStyle?: object | undefined;
  subheader: string;
  subheaderStyle?: object | undefined;
  borderColor?: string | 'green';
  onClick: any;
}

export default IQuickStartCard;
