import React from 'react';
import {View} from 'react-native';
import IntervalCardHeader from '../../atoms/IntervalCardHeader/IntervalCardHeader';
import StartButton from '../../atoms/StartButton/StartButton';
import IQuickStartCard from './IQuickStartCard';
import createStyles from './QuickStartCardStyle';

const QuickStartCard = (props: IQuickStartCard) => {
  // Create styles
  const styles = createStyles();

  // Return TSX
  return (
    <View style={styles.container}>
      <IntervalCardHeader
        header={props.header}
        headerStyle={props.headerStyle}
        subheader={props.subheader}
        subheaderStyle={props.subheaderStyle}
      />
      <StartButton
        text={'Start'}
        color={props.borderColor}
        onClick={() => console.log('QuickStart ' + props.header + ' Clicked.')}
      />
    </View>
  );
};

export default QuickStartCard;
