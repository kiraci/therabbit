interface IIntervalCard {
  header: string;
  headerStyle?: object | undefined;
  subheader: string;
  subheaderStyle?: object | undefined;
  description: string;
  icon: string;
}

export default IIntervalCard;
