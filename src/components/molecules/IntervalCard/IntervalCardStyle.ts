import Colors from '../../../constants/colors';
import {StyleSheet} from 'react-native';
import IIntervalCard from './IIntervalCard';

const createStyles = (props: IIntervalCard, selected: boolean) =>
  StyleSheet.create({
    container: {
      flexDirection: 'column',
      justifyContent: 'center',
      padding: 10,
      backgroundColor: Colors.grey700,
      borderRadius: 10,
      margin: 10,
      marginLeft: 0,
      width: 200,
      height: 150,
      borderWidth: 5,
      borderColor: selected ? Colors.indigoA700 : Colors.grey700,
    },
  });

export default createStyles;
