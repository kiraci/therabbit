import React, {useState} from 'react';
import {Pressable} from 'react-native';
import IntervalCardBody from '../../atoms/IntervalCardBody/IntervalCardBody';
import IntervalCardHeader from '../../atoms/IntervalCardHeader/IntervalCardHeader';
import IntervalIcon from '../../atoms/IntervalIcon/IntervalIcon';
import IIntervalCard from './IIntervalCard';
import createStyles from './IntervalCardStyle';

const IntervalCard = (props: IIntervalCard) => {
  // Use state
  const [selected, setSelected] = useState(false);

  // Create styles
  const styles = createStyles(props, selected);

  // On press function
  const onPress = () => {
    setSelected(!selected);
  };

  // Return TSX
  return (
    <Pressable style={styles.container} onPress={onPress}>
      <IntervalCardHeader
        header={props.header}
        headerStyle={props.headerStyle}
        subheader={props.subheader}
        subheaderStyle={props.subheaderStyle}
      />
      <IntervalCardBody description={props.description} />
      <IntervalIcon icon={props.icon} size={30} right={10} top={10} />
    </Pressable>
  );
};

export default IntervalCard;
