import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import CircleProps from './ICircle';

const Circle = ({initial, color, float}: CircleProps) => {
  const styles = StyleSheet.create({
    container: {
      width: 50,
      height: 50,
      borderRadius: 50,
      backgroundColor: color,
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf: float,
    } as any,
    textStyle: {
      color: 'white',
    },
  });

  return (
    <View style={styles.container}>
      <Text style={styles.textStyle}>{initial}</Text>
    </View>
  );
};

export default Circle;
