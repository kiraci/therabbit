interface CircleProps {
  initial: string;
  color: string;
  float?: string | undefined;
}

export default CircleProps;
