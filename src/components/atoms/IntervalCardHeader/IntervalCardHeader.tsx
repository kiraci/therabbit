import React from 'react';
import {Text, View} from 'react-native';
import IIntervalCardHeader from './IIntervalCardHeader';
import createStyles from './IntervalCardHeaderStyle';

const IntervalCardHeader = (props: IIntervalCardHeader) => {
  // Create styles
  const styles = createStyles();

  // Return TSX
  return (
    <View style={styles.container}>
      <Text style={[styles.header, props.headerStyle]}>{props.header}</Text>
      <Text style={[styles.subheader, props.subheaderStyle]}>
        {props.subheader}
      </Text>
    </View>
  );
};

export default IntervalCardHeader;
