import {StyleSheet} from 'react-native';
import Colors from '../../../constants/colors';

const createStyles = () =>
  StyleSheet.create({
    container: {
      flexDirection: 'column',
      justifyContent: 'space-between',
      padding: 10,
      backgroundColor: Colors.grey800,
    },
    header: {
      fontSize: 20,
      fontWeight: 'bold',
      color: Colors.grey900,
    },
    subheader: {
      fontSize: 12,
      color: Colors.grey900,
    },
  });

export default createStyles;
