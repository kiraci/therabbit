interface IIntervalCardHeader {
  header: string;
  headerStyle?: object | undefined;
  subheader: string;
  subheaderStyle?: object | undefined;
}

export default IIntervalCardHeader;
