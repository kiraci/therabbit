import {StyleSheet} from 'react-native';
import IFloatingButton from './IFloatingButton';

const createStyles = (props: IFloatingButton) =>
  StyleSheet.create({
    container: {
      width: props.size,
      height: props.size,
      borderRadius: 50,
      backgroundColor: props.color,
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      bottom: props.bottom,
      right: props.right,
      left: props.left,
      top: props.top,
    },
    imageStyle: {
      width: 30,
      height: 30,
    },
    textStyle: {
      color: 'white',
      fontSize: 20,
    },
  });

export default createStyles;
