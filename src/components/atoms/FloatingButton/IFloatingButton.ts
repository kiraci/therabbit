interface FloatingButtonProps {
  size: number;
  color: string;
  icon?: string;
  initial?: string | undefined;
  top?: number;
  right?: number;
  bottom?: number;
  left?: number;
}

export default FloatingButtonProps;
