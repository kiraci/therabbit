import React from 'react';
import {Image, Text, TouchableOpacity} from 'react-native';
import FloatingButtonProps from './IFloatingButton';
import createStyles from './FloatingButtonStyle';

const FloatingButton = (props: FloatingButtonProps) => {
  // Create styles
  const styles = createStyles(props);

  // Set icon path
  var iconPath =
    props.icon === 'start'
      ? require('../../../assets/images/start.png')
      : require('../../../assets/images/plus.png');

  // Return TSX
  return (
    <TouchableOpacity style={styles.container}>
      {props.initial && <Text style={styles.textStyle}>{props.initial}</Text>}

      {props.icon && <Image style={styles.imageStyle} source={iconPath} />}
    </TouchableOpacity>
  );
};

export default FloatingButton;
