import React from 'react';
import {Text} from 'react-native';
import HeaderProps from './IHeader';

const Header = ({header, fontSize, color, float}: HeaderProps) => {
  const styles = {
    fontSize: fontSize,
    color: color,
    textAlign: float,
  };

  return <Text style={styles as any}>{header}</Text>;
};

export default Header;
