interface HeaderProps {
  header: string;
  fontSize?: number | undefined;
  color?: string | undefined;
  float?: string | undefined;
}

export default HeaderProps;
