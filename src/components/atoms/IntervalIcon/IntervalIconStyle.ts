import {StyleSheet} from 'react-native';
import IIntervalIcon from './IIntervalIcon';

const createStyles = (props: IIntervalIcon) =>
  StyleSheet.create({
    icon: {
      position: 'absolute',
      top: props.top,
      right: props.right,
      left: props.left,
      bottom: props.bottom,
      width: props.size,
      height: props.size,
    },
  });

export default createStyles;
