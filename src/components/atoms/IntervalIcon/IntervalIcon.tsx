import React from 'react';
import {Image} from 'react-native';
import IIntervalIcon from './IIntervalIcon';
import createStyles from './IntervalIconStyle';

const IntervalIcon = (props: IIntervalIcon) => {
  // Create styles
  const styles = createStyles(props);

  // Set icon path
  const iconPath = () => {
    switch (props.icon) {
      case 'workout':
        return require('../../../assets/images/workout.png');
      case 'study':
        return require('../../../assets/images/study.png');
      default:
        return require('../../../assets/images/default-interval.png');
    }
  };

  // Return TSX
  return <Image style={styles.icon} source={iconPath()} />;
};

export default IntervalIcon;
