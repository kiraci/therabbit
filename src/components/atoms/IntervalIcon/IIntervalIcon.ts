interface IIntervalIcon {
  top?: number;
  right?: number;
  left?: number;
  bottom?: number;
  size: number;
  icon: string;
}

export default IIntervalIcon;
