interface IStartButton {
  text: string;
  color?: string | 'green';
  onClick: () => void;
}

export default IStartButton;
