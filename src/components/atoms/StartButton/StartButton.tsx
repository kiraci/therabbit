import React from 'react';
import {Pressable, Text} from 'react-native';
import IStartButton from './IStartButton';
import createStyles from './StartButtonStyle';

const StartButton = (props: IStartButton) => {
  // Create styles
  const styles = createStyles(props);

  // Return TSX
  return (
    <Pressable style={styles.container} onPress={props.onClick}>
      <Text style={styles.text}>{props.text}</Text>
    </Pressable>
  );
};

export default StartButton;
