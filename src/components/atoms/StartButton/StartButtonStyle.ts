import {StyleSheet} from 'react-native';
import IStartButton from './IStartButton';

const createStyles = (props: IStartButton) =>
  StyleSheet.create({
    container: {
      width: 75,
      height: 75,
      borderRadius: 50,
      borderWidth: 1,
      borderColor: props.color || 'green',
      alignItems: 'center',
      justifyContent: 'center',
    },
    text: {
      color: props.color || 'green',
      alignSelf: 'center',
    },
  });

export default createStyles;
