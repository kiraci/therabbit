import Colors from '../../../constants/colors';
import {StyleSheet} from 'react-native';

const createStyles = () =>
  StyleSheet.create({
    container: {
      flexDirection: 'row',
      justifyContent: 'flex-start',
      padding: 10,
      backgroundColor: Colors.grey800,
    },
    description: {
      fontSize: 14,
      color: Colors.grey300,
    },
  });

export default createStyles;
