import React from 'react';
import {View, Text} from 'react-native';
import createStyles from '../IntervalCardBody/IntervalCardBodyStyle';
import IIntervalCardBody from './IIntervalCardBody';

const IntervalCardBody = (props: IIntervalCardBody) => {
  // Create styles
  const styles = createStyles();

  // Return TSX
  return (
    <View style={styles.container}>
      <Text style={styles.description}>{props.description}</Text>
    </View>
  );
};

export default IntervalCardBody;
