import React from 'react';
import {FlatList, View} from 'react-native';
import Header from '../../atoms/Header/Header';
import IntervalCard from '../../molecules/IntervalCard/IntervalCard';
import QuickStartCard from '../../molecules/QuickStartCard/QuickStartCard';
import IIntervalCardSlider from './IIntervalCardSlider';

const IntervalCardSlider = (props: IIntervalCardSlider) => {
  return (
    <View>
      <Header
        header={props.header}
        float={'left'}
        color={'white'}
        fontSize={24}
      />
      <FlatList
        data={props.data}
        renderItem={({item}) =>
          props.type === 'IntervalCard' ? (
            <IntervalCard
              header={item.header}
              headerStyle={props.headerStyle}
              subheaderStyle={props.subheaderStyle}
              icon={item.icon}
              subheader={item.subheader}
              description={item.description}
            />
          ) : (
            <QuickStartCard
              header={item.header}
              headerStyle={props.headerStyle}
              subheader={item.subheader}
              subheaderStyle={props.subheaderStyle}
              borderColor={props.borderColor}
              onClick={() => console.log('Clicked')}
            />
          )
        }
        horizontal={true}
      />
    </View>
  );
};

export default IntervalCardSlider;
