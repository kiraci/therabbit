interface IIntervalCardSlider {
  header: string;
  headerStyle?: object | undefined;
  subheaderStyle?: object | undefined;
  data: any;
  borderColor?: string | 'green';
  type: string;
}

export default IIntervalCardSlider;
