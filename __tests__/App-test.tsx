/**
 * I need to create a testing stragety for my app
 */

import 'react-native';
import React from 'react';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import FloatingButton from '../src/components/atoms/FloatingButton/FloatingButton';

// Test if App renders correctly
// It doesn't work becaue of the carousel's implementation syntax
/*
      it('renders correctly', () => {
        renderer.create(<App />);
      });
*/

// Unit test for FloatingButton
it('renders correctly', () => {
  renderer.create(
    <FloatingButton
      size={42}
      color={'#3F51B5'}
      initial={'O'}
      right={10}
      top={8}
    />,
  );
});
