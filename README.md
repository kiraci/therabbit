# TheRabbit

## Name
The Rabbit
- since i am an animal lover, projects in this account will be devoted an animal as they inspire me to do so. 

## Description
This app enables you to create custom time interval that you can start, store and repeat for certain tasks or routines in daily life. For instance, you have a workout routine that consists sets and breaks. You can predetermine time for each set and break, then, by pressing start button, you can start your routine without distraction.  

## Installation
You will be able to download app in Google Play Store and Apple Store.

## Contributing
Contact to contribute.

## Authors and acknowledgment
kiraci

## License
It is an open source project but license is not decided yet.

## Project status
In development.
